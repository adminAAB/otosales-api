require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Generate Report Komisi MGO (Upliner)", function () {

   before(async function () {
      await initial.login(process.env.USER_AGENCYMGR, process.env.PASS, 1);
  });

   //Test Case 1
   it("Should passed (Generate Report Komisi MGO (Upliner) Monthly) #20220127000830", async function () {
      let response = await api_general.GenerateReport("2", "2022-01-01", "2022-01-31");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })
})