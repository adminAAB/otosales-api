require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral

describe("Test Suite Login Pass (All Role)", function () {
   //Test Case 1
   it("Should passed (Login by Admin Agency) #20222701000000", async function () {
      let response = await api_general.Login(process.env.USER_ADMIN, process.env.PASS, 1);
      response.should.have.status(200);
      response.body.status.should.equal(true);
      response.body.errorCode.should.equal(0);
      response.body.token.should.not.equal(null);
   })
   
   //Test Case 2
   it("Should passed (Login by Section Head) #20222701000030", async function () {
      let response = await api_general.Login(process.env.USER_SECTHEAD, process.env.PASS, 1);
      response.should.have.status(200);
      response.body.status.should.equal(true);
      response.body.errorCode.should.equal(0);
      response.body.token.should.not.equal(null);
   })
   
   //Test Case 3
   it("Should passed (Login by Agency Manager) #20222701000100", async function () {
      let response = await api_general.Login(process.env.USER_AGENCYMGR, process.env.PASS, 1);
      response.should.have.status(200);
      response.body.status.should.equal(true);
      response.body.errorCode.should.equal(0);
      response.body.token.should.not.equal(null);
   })
})