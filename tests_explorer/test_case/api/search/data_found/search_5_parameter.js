const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (5 Parameter)", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and AgentLevel) #20220127013700", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and AgentRole) #20220127013730", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and AgentType) #20220127013800", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and Evaluation Status) #20220127013830", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, and AgentRole) #20220127013900", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, and AgentType) #20220127013930", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, and Evaluation Status) #20220127014000", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })


   //Test Case 8
   it("Should passed (Search by XAgentID, AgentID, Name, AgentRole, and AgentType) #20220127014030", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })


   //Test Case 9
   it("Should passed (Search by XAgentID, AgentID, Name, AgentRole, and Evaluation Status) #20220127014100", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })


   //Test Case 10
   it("Should passed (Search by XAgentID, AgentID, Name, AgentType, and Evaluation Status) #20220127014130", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, and AgentRole) #20220127014200", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, and AgentType) #20220127014230", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, and Evaluation Status) #20220127014300", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })


   //Test Case 14
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentRole, and AgentType) #20220127014330", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentRole, and Evaluation Status) #20220127014400", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentType, and Evaluation Status) #20220127014430", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, AgentRole, and AgentType) #20220127014500", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, AgentRole, and Evaluation Status) #20220127014530", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, AgentType, and Evaluation Status) #20220127014600", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by XAgentID, AgentID, AgentRole, AgentType, and Evaluation Status) #20220127014630", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, and AgentRole) #20220127014700", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, and AgentType) #20220127014730", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, and Evaluation Status) #20220127014800", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentRole, and AgentType) #20220127014830", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentRole, and Evaluation Status) #20220127014900", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentType, and Evaluation Status) #20220127014930", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })
   
   //Test Case 27
   it("Should passed (Search by XAgentID, Name, AgentLevel, AgentRole, and AgentType) #20220127015000", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by XAgentID, Name, AgentLevel, AgentRole, and Evaluation Status) #20220127015030", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 29
   it("Should passed (Search by XAgentID, Name, AgentLevel, AgentType, and Evaluation Status) #20220127015100", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 30
   it("Should passed (Search by XAgentID, Name, AgentRole, AgentType, and Evaluation Status) #20220127015130", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 31
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, AgentRole, and AgentType) #20220127015200", async function () {
      let response = await api_general.Search("JESS0014", "", "", "022", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 32
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, AgentRole, and Evaluation Status) #20220127015230", async function () {
      let response = await api_general.Search("JESS0014", "", "", "022", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 33
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, AgentType, and Evaluation Status) #20220127015300", async function () {
      let response = await api_general.Search("JESS0014", "", "", "022", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 34
   it("Should passed (Search by XAgentID, BranchCode, AgentRole, AgentType, and Evaluation Status) #20220127015330", async function () {
      let response = await api_general.Search("JESS0014", "", "", "022", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 35
   it("Should passed (Search by XAgentID, AgentLevel, AgentRole, AgentType, and Evaluation Status) #20220127015400", async function () {
      let response = await api_general.Search("JESS0014", "", "", "", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 36
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, and AgentRole) #20220127015430", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 37
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, and AgentType) #20220127015500", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 38
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, and Evaluation Status) #20220127015530", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 39
    it("Should passed (Search by AgentID, Name, BranchCode, AgentRole, and AgentType) #20220127015600", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 40
    it("Should passed (Search by AgentID, Name, BranchCode, AgentRole, and Evaluation Status) #20220127015630", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 41
   it("Should passed (Search by AgentID, Name, BranchCode, AgentType, and Evaluation Status) #20220127015700", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 42
   it("Should passed (Search by AgentID, Name, AgentLevel, AgentRole, and AgentType) #20220127015730", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 43
   it("Should passed (Search by AgentID, Name, AgentLevel, AgentRole, and Evaluation Status) #20220127015800", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 44
    it("Should passed (Search by AgentID, Name, AgentLevel, AgentType, and Evaluation Status) #20220127015830", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 45
   it("Should passed (Search by AgentID, Name, AgentRole, AgentType, and Evaluation Status) #20220127015900", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 46
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, AgentRole, and AgentType) #20220127015930", async function () {
      let response = await api_general.Search("", "00000014", "", "022", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 47
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, AgentRole, and Evaluation Status) #20220127020000", async function () {
      let response = await api_general.Search("", "00000014", "", "022", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 48
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, AgentType, and Evaluation Status) #20220127020030", async function () {
      let response = await api_general.Search("", "00000014", "", "022", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 49
   it("Should passed (Search by AgentID, BranchCode, AgentRole, AgentType, and Evaluation Status) #20220127020100", async function () {
      let response = await api_general.Search("", "00000014", "", "022", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 50
   it("Should passed (Search by AgentID, AgentLevel, AgentRole, AgentType, and Evaluation Status) #20220127020130", async function () {
      let response = await api_general.Search("", "00000014", "", "", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 51
   it("Should passed (Search by Name, BranchCode, AgentLevel, AgentRole, and AgentType) #20220127020200", async function () {
      let response = await api_general.Search("", "", "Lily Juliana", "022", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 52
    it("Should passed (Search by Name, BranchCode, AgentLevel, AgentRole, and Evaluation Status) #20220127020230", async function () {
      let response = await api_general.Search("", "", "Lily Juliana", "022", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 53
   it("Should passed (Search by Name, BranchCode, AgentLevel, AgentType, and Evaluation Status) #20220127020300", async function () {
      let response = await api_general.Search("", "", "Lily Juliana", "022", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 54
   it("Should passed (Search by Name, BranchCode, AgentRole, AgentType, and Evaluation Status) #20220127020330", async function () {
      let response = await api_general.Search("", "", "Lily Juliana", "022", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 55
   it("Should passed (Search by Name, AgentLevel, AgentRole, AgentType, and Evaluation Status) #20220127020400", async function () {
      let response = await api_general.Search("", "", "Lily Juliana", "", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 56
   it("Should passed (Search by BranchCode, AgentLevel, AgentRole, AgentType, Evaluation Status) #20220127020430", async function () {
      let response = await api_general.Search("", "", "", "022", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })
})