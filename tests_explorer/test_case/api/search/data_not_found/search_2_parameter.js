const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (2 Parameter) Data Not Found", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID and AgentID)", async function () {
      let response = await api_general.Search("JESS0015", "20000014", "", "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID and Name)", async function () {
      let response = await api_general.Search("JESS0015", "", "Lily", "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID and BranchCode)", async function () {
      let response = await api_general.Search("JESS0015", "", "", "122", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID and Agent Level)", async function () {
      let response = await api_general.Search("JESS0015", "", "", "", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID and AgentRole)", async function () {
      let response = await api_general.Search("JESS0015", "", "", "", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID and AgentType)", async function () {
      let response = await api_general.Search("JESS0015", "", "", "", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0015", "", "", "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by AgentID and Name)", async function () {
      let response = await api_general.Search("", "20000014", "Lily Lolililo", "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 9
   it("Should passed (Search by AgentID and BranchCode", async function(){
      let response = await api_general.Search("","20000014","","122","","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 10
   it("Should passed (Search by AgentID and AgentLevel", async function(){
      let response = await api_general.Search("","20000014","","","1","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by AgentID and AgentRole", async function(){
      let response = await api_general.Search("","20000014","","","","1","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by AgentID and AgentType", async function(){
      let response = await api_general.Search("","20000014","","","","","1","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by AgentID and Evaluation Status", async function(){
      let response = await api_general.Search("","20000014","","","","","","1","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 14
   it("Should passed (Search by Name and BranchCode", async function(){
      let response = await api_general.Search("","","Lily Lolililo","122","","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by Name and AgentLevel", async function(){
      let response = await api_general.Search("","","Lily Lolililo","","1","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by Name and AgentRole", async function(){
      let response = await api_general.Search("","","Lily Lolililo","","","1","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by Name and AgentType", async function(){
      let response = await api_general.Search("","","Lily Lolililo","","","","1","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by Name and Evaluation Status", async function(){
      let response = await api_general.Search("","","Lily Lolililo","","","","","1","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by BranchCode and AgentLevel", async function(){
      let response = await api_general.Search("","","","122","1","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by BranchCode and AgentRole", async function(){
      let response = await api_general.Search("","","","122","","1","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by BranchCode and AgentType", async function(){
      let response = await api_general.Search("","","","122","","","1","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by BranchCode and Evaluation Status", async function(){
      let response = await api_general.Search("","","","122","","","","1","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by AgentLevel and AgentRole", async function(){
      let response = await api_general.Search("","","","","100","100","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by AgentLevel and AgentType", async function(){
      let response = await api_general.Search("","","","","100","","100","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by AgentLevel and Evaluation Status", async function(){
      let response = await api_general.Search("","","","","100","","","100","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by AgentRole and AgentType", async function(){
      let response = await api_general.Search("","","","","","100","100","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 27
   it("Should passed (Search by AgentRole and Evaluation Status", async function(){
      let response = await api_general.Search("","","","","","100","","100","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by AgentType and Evaluation Status", async function(){
      let response = await api_general.Search("","","","","","","100","100","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })
})