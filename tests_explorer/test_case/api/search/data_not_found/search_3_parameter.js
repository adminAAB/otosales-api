const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (3 Parameter) Data Not Found", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID, AgentID, and Name)", async function () {
      let response = await api_general.Search("JESS10014", "10000014", "Lily Halalilulo", "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID, AgentID, and BranchCode)", async function () {
      let response = await api_general.Search("JESS10014", "10000014", "", "122", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID, AgentID, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS10014", "10000014", "", "", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID, AgentID, and AgentRole)", async function () {
      let response = await api_general.Search("JESS10014", "10000014", "", "", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID, AgentID, and AgentType)", async function () {
      let response = await api_general.Search("JESS10014", "10000014", "", "", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID, AgentID, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS10014", "10000014", "", "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID, Name, and BranchCode)", async function () {
      let response = await api_general.Search("JESS10014", "", "Lily Halalilulo", "122", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by XAgentID, Name, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS10014", "", "Lily Halalilulo", "", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 9
   it("Should passed (Search by XAgentID, Name, and AgentRole)", async function () {
      let response = await api_general.Search("JESS10014", "", "Lily Halalilulo", "", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 10
   it("Should passed (Search by XAgentID, Name, and AgentType)", async function () {
      let response = await api_general.Search("JESS10014", "", "Lily Halalilulo", "", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by XAgentID, Name, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS10014", "", "Lily Halalilulo", "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by XAgentID, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by XAgentID, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })


   //Test Case 14
   it("Should passed (Search by XAgentID, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by XAgentID, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by XAgentID, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by XAgentID, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by XAgentID, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by XAgentID, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by XAgentID, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by XAgentID, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS10014", "", "", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by AgentID, Name, and BranchCode)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Halalilulo", "122", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by AgentID, Name, and AgentLevel)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Halalilulo", "", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by AgentID, Name, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Halalilulo", "", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by AgentID, Name, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Halalilulo", "", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by AgentID, Name, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Halalilulo", "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 27
   it("Should passed (Search by AgentID, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by AgentID, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 29
   it("Should passed (Search by AgentID, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 30
   it("Should passed (Search by AgentID, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 31
   it("Should passed (Search by AgentID, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 32
   it("Should passed (Search by AgentID, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 33
   it("Should passed (Search by AgentID, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 34
   it("Should passed (Search by AgentID, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 35
   it("Should passed (Search by AgentID, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 36
   it("Should passed (Search by AgentID, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 37
   it("Should passed (Search by Name, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 38
   it("Should passed (Search by Name, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 39
    it("Should passed (Search by Name, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 40
    it("Should passed (Search by Name, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 41
   it("Should passed (Search by Name, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 42
   it("Should passed (Search by Name, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 43
   it("Should passed (Search by Name, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 44
    it("Should passed (Search by Name, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 45
   it("Should passed (Search by Name, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 46
   it("Should passed (Search by Name, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Halalilulo", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 47
   it("Should passed (Search by BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 48
   it("Should passed (Search by BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 49
   it("Should passed (Search by BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 50
   it("Should passed (Search by BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 51
   it("Should passed (Search by BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 52
    it("Should passed (Search by BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 53
   it("Should passed (Search by AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "", "", "100", "100", "100", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 54
   it("Should passed (Search by AgentLevel, AgentRole, and EvaluationStatus)", async function () {
      let response = await api_general.Search("", "", "", "", "100", "100", "", "100", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 55
   it("Should passed (Search by AgentLevel, AgentType, and EvaluationStatus)", async function () {
      let response = await api_general.Search("", "", "", "", "100", "", "100", "100", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 56
   it("Should passed (Search by AgentRole, AgentType, and EvaluationStatus)", async function () {
      let response = await api_general.Search("", "", "", "", "", "100", "100", "100", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })
})