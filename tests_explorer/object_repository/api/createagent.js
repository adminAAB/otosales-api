const ApiBase = require(`${process.cwd()}/tests_explorer/object_repository/api_base`)       // Harus selalu ada

class CreateAgent extends ApiBase {                                                       // Sesuaikan nama module nya
    SaveAgentInfo(IsMGOBid, RegNo, AifID, XAgentID, Name, AgentNameBukuTabungan, NName, BranchCode, AgentType, RegDate, AAUICertNo,
        AAUICertExpDate, PKSNo, PKSExpDate, UAgentID, GomitraEmail, AppDate, BankID, BankName, BankBranchID, BankBranch, BankAccount, 
        AccountName, NPWPNo, NPWPName, NPWPAddress, NPWPDate, Dependant, IsLeader, ReferenceAgent, AIFStatus, IdentityType, IdentityNo, BirthPlace,
        BirthDate, Gender, Marital, CitizenShip, Religion, MobilePhone, MobilePhone2, MobilePhone3, Email, HAddress, HAddress2, HCity, HZipCode,
        HPhone, OAddress, OCity, OZipCode, OExtPhone, OEMail, RName, RAddress, RCity, RZipCode, RPhone, RMobilePhone, REMail, FName, FAddress,
        FCity, FZipCode, FPhone, FMobilePhone, FEMail, EduLevel, Institution, Program, City, EduSDate, EduEDate, WETitle, WECompany,WEWExSDate,
        WEWExEDate, CMCourseName, CMOrganizer, CMCategory, CMResult, CMCourseSDate, CMCourseEDate, UserID, UserRole, ApprovalType) {                                                    // Sesuaikan nama function dengan nama API, parameternya disesuaikan juga
        return this.endpoint.post('/API/AgencyInformationFiles/SaveAgentInfo')         
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "AIFModel[IsMGOBid]": IsMGOBid,
                "AIFModel[RegNo]": RegNo,
                "AIFModel[ID]": AifID,
                "AIFModel[XAgentID]": XAgentID,
                "AIFModel[Name]": Name,
                "AIFModel[AgentNameBukuTabungan]": AgentNameBukuTabungan,
                "AIFModel[NName]": NName,
                "AIFModel[BranchCode]": BranchCode,
                "AIFModel[AgentType]": AgentType,
                "AIFModel[RegDate]": RegDate,
                "AIFModel[AAUICertNo]": AAUICertNo,
                "AIFModel[AAUICertExpDate]": AAUICertExpDate,
                "AIFModel[PKSNo]": PKSNo,
                "AIFModel[PKSExpDate]": PKSExpDate,
                "AIFModel[UAgentID]": UAgentID,
                "AIFModel[GomitraEmail]": GomitraEmail,
                "AIFModel[AppDate]": AppDate,
                "AIFModel[BankID]": BankID,
                "AIFModel[BankName]": BankName,
                "AIFModel[BankBranchID]": BankBranchID,
                "AIFModel[BankBranch]": BankBranch,
                "AIFModel[BankAccount]": BankAccount,
                "AIFModel[AccountName]": AccountName,
                "AIFModel[NPWPNo]": NPWPNo,
                "AIFModel[NPWPName]": NPWPName,
                "AIFModel[NPWPAddress]": NPWPAddress,
                "AIFModel[NPWPDate]": NPWPDate,
                "AIFModel[Dependant]": Dependant,
                "AIFModel[IsLeader]": IsLeader,
                "AIFModel[ReferenceAgent]": ReferenceAgent,
                "AIFModel[AIFStatus]" : AIFStatus,
                "AIFPersonalModel[IdentityType]": IdentityType,
                "AIFPersonalModel[IdentityNo]": IdentityNo,
                "AIFPersonalModel[BirthPlace]": BirthPlace,
                "AIFPersonalModel[BirthDate]": BirthDate,
                "AIFPersonalModel[Gender]": Gender,
                "AIFPersonalModel[Marital]": Marital,
                "AIFPersonalModel[CitizenShip]": CitizenShip,
                "AIFPersonalModel[Religion]": Religion,
                "AIFPersonalModel[MobilePhone]": MobilePhone,
                "AIFPersonalModel[MobilePhone2]": MobilePhone2,
                "AIFPersonalModel[MobilePhone3]": MobilePhone3,
                "AIFPersonalModel[Email]": Email,
                "AIFPersonalModel[HAddress]": HAddress,
                "AIFPersonalModel[HAddress2]": HAddress2,
                "AIFPersonalModel[HCity]": HCity,
                "AIFPersonalModel[HZipCode]": HZipCode,
                "AIFPersonalModel[HPhone]": HPhone,
                "AIFPersonalModel[OAddress]": OAddress,
                "AIFPersonalModel[OCity]": OCity,
                "AIFPersonalModel[OZipCode]": OZipCode,
                "AIFPersonalModel[OExtPhone]": OExtPhone,
                "AIFPersonalModel[OEMail]": OEMail,
                "AIFPersonalModel[RName]": RName,
                "AIFPersonalModel[RAddress]": RAddress,
                "AIFPersonalModel[RCity]": RCity,
                "AIFPersonalModel[RZipCode]": RZipCode,
                "AIFPersonalModel[RPhone]": RPhone,
                "AIFPersonalModel[RMobilePhone]": RMobilePhone,
                "AIFPersonalModel[REMail]": REMail,
                "AIFPersonalModel[FName]": FName,
                "AIFPersonalModel[FAddress]": FAddress,
                "AIFPersonalModel[FCity]": FCity,
                "AIFPersonalModel[FZipCode]": FZipCode,
                "AIFPersonalModel[FPhone]": FPhone,
                "AIFPersonalModel[FMobilePhone]": FMobilePhone,
                "AIFPersonalModel[FEMail]": FEMail,
                "AIFEducationModel[EduLevel]": EduLevel,
                "AIFEducationModel[Institution]": Institution,
                "AIFEducationModel[Program]": Program,
                "AIFEducationModel[City]": City,
                "AIFEducationModel[EduSDate]": EduSDate,
                "AIFEducationModel[EduEDate]": EduEDate,
                "ListAIFWorkingExpModel[0][Title]": WETitle,
                "ListAIFWorkingExpModel[0][Company]": WECompany,
                "ListAIFWorkingExpModel[0][WExSDate]": WEWExSDate,
                "ListAIFWorkingExpModel[0][WExEDate]": WEWExEDate,
                "ListAIFCourseModel[0][CourseName]": CMCourseName,
                "ListAIFCourseModel[0][Organizer]": CMOrganizer,
                "ListAIFCourseModel[0][Category]": CMCategory,
                "ListAIFCourseModel[0][Result]": CMResult,
                "ListAIFCourseModel[0][CourseSDate]": CMCourseSDate,
                "ListAIFCourseModel[0][CourseEDate]": CMCourseEDate,
                "UserID": UserID,
                "UserRole": UserRole,
                "ApprovalType": ApprovalType
            })
    }

    GetEmailGomitra(ParamSearch) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/GetEmailGomitra')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "ParamSearch": ParamSearch
            })
    }

    UploadData(pDataFiles0, pCountDataFiles, pObjectNo, pReferenceImageType, pImageReferenceNo, pImageIsValid, pTempOrderNoIsValid, pApprovalID) {                                                                // Contoh upload image atau file
        return this.endpoint.post('/API/AgencyInformationFiles/UploadImage')
            .set('content-type', 'multipart/form-data')
            .set('authorization', 'Bearer ' + global.token)
            .set('cookie', 'EGRUM_BTM=c4917d71-6484-483f-84d0-e9d1b0fb9ade#~#1||0; TS0125c461=015294299a971cf504b5cbde464648ec31fcb5d3a1b3f432c0687217bf68f34f9353dab9bbe22536e6c21445ef511486c290d5a0ff2fe87f815f2527a12e1bab19be2d150b')
            .attach('pDataFiles0', pDataFiles0)
            .field('pCountDataFiles', pCountDataFiles)
            .field('pObjectNo', pObjectNo)
            .field('pReferenceImageType', pReferenceImageType)
            .field('pImageReferenceNo', pImageReferenceNo)
            .field('pImageIsValid', pImageIsValid)
            .field('pTempOrderNoIsValid', pTempOrderNoIsValid)
            .field('pApprovalID', pApprovalID)
    }

    GetOldLeader(ParamSearch) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/GetOldLeader')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "ParamSearch": ParamSearch
            })
    }

    SaveApproval(ApprovalID, UserRole, UserID, ApprovalType, ApprovalStatus, Remark) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/SaveApproval')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "ApprovalID": ApprovalID,
                "UserRole": UserRole,
                "UserID": UserID,
                "ApprovalType": ApprovalType,
                "ApprovalStatus": ApprovalStatus,
                "Remark": Remark
            })
    }

    SaveToAIF(XAgentID, ApprovalID) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/SaveToAIF')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "XAgentID": XAgentID,                                                                     
                "ApprovalID": ApprovalID
            })
    }

    SaveSalesOfficer(SalesOfficerID, Email, Name, Role, Class, Password, Validation, Expiry, Password_Expiry, Department, 
        BranchCode, Phone1, Phone2, Phone3, OfficePhone, Ext, Fax, IsSendBid){                                                   
        return this.endpoint.post('/API/TitanToAABMobile/SaveSalesOfficer')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "SalesOfficerID": SalesOfficerID,
                "Email": Email,
                "Name": Name,
                "Role": Role,
                "Class": Class,
                "Password": Password,
                "Validation": Validation,
                "Expiry": Expiry,
                "Password_Expiry": Password_Expiry,
                "Department": Department,
                "BranchCode": BranchCode,
                "Phone1": Phone1,
                "Phone2": Phone2,
                "Phone3": Phone3,
                "OfficePhone": OfficePhone,
                "Ext": Ext,
                "Fax:": Fax,
                "IsSendBid": IsSendBid
            })
    }

    SaveExternalUser(UserID, FirstName, LastName, ActivationStatus, ExpiryDate, LastLoginDate, AccumulatedLoginFailedCount, MobileNumber, 
        CreatedBy, IsMGOBid){                                                   
        return this.endpoint.post('/API/TitanToAuthorizationDB/SaveExternalUser')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "UserID": UserID,
                "FirstName": FirstName,
                "LastName": LastName,
                "ActivationStatus": ActivationStatus,
                "ExpiryDate": ExpiryDate,
                "LastLoginDate": LastLoginDate,
                "AccumulatedLoginFailedCount": AccumulatedLoginFailedCount,
                "MobileNumber": MobileNumber,
                "CreatedBy": CreatedBy,
                "IsMGOBid": IsMGOBid
            })
    }

    SaveCustomer(UserID, Cust_Id, Name, Cust_Type, Email, BirthDate, HomeAddress, HomePostalCode, HomeNumber, OfficeAddress, 
        OfficePostalCode, OfficeNumber, OfficeExt, UplinerClientCode, LeaderClientCode, HandPhone, HandPhone2, HandPhone3, HandPhone4, Company_Name,
        Job_Title, Sex, Marital, Religion, id_card){                                                   
        return this.endpoint.post('/API/TitanToAAB/SaveCustomer')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({                                                                         
                "UserID": UserID,
                "Customer[Cust_Id]": Cust_Id,
                "Customer[Name]": Name,
                "Customer[Cust_Type]": Cust_Type,
                "Customer[Email]": Email,
                "Customer[BirthDate]": BirthDate,
                "Customer[HomeAddress]": HomeAddress,
                "Customer[HomePostalCode]": HomePostalCode,
                "Customer[HomeNumber]": HomeNumber,
                "Customer[OfficeAddress]": OfficeAddress,
                "Customer[OfficePostalCode]": OfficePostalCode,
                "Customer[OfficeNumber]": OfficeNumber,
                "Customer[OfficeExt]": OfficeExt,
                "Customer[UplinerClientCode]": UplinerClientCode,
                "Customer[LeaderClientCode]": LeaderClientCode,
                "Cust_Personal[HP]": HandPhone,
                "Cust_Personal[HP_2]:": HandPhone2,
                "Cust_Personal[HP_3]": HandPhone3,
                "Cust_Personal[HP_4]": HandPhone4,
                "Cust_Personal[Company_Name]": Company_Name,
                "Cust_Personal[Job_Title]": Job_Title,
                "Cust_Personal[Sex]": Sex,
                "Cust_Personal[Marital]": Marital,
                "Cust_Personal[Religion]": Religion,
                "Cust_Personal[id_card]": id_card
            })
    }

    RecordAgentHierarchyMonthly(ParamSearch) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/RecordAgentHierarchyMonthly')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "ParamSearch": ParamSearch
            })
    }

    UpdatePassword(XAgentID, GomitraEmail) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/UpdatePassword')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "XAgentID": XAgentID,                                                                     
                "GomitraEmail": GomitraEmail
            })
    }

    SendAutoEmailAccountGomitra(ApprovalID, UserRole) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/SendAutoEmailAccountGomitra')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "ApprovalID": ApprovalID,                                                                     
                "UserRole": UserRole
            })
    }

    EmailDetailApprovalNotification(ApprovalID) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/EmailDetailApprovalNotification')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "ApprovalID": ApprovalID
            })
    }

    SendEmailNotification(EmailBody, EmailSubject, UserID, ApprovalCode, ApprovalStatus, ApprovalType, ApprovalUserID) {                                                   
        return this.endpoint.post('/API/TitanToAuthorizationDB/SendEmailNotification')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "EmailBody": EmailBody,
                "EmailSubject": EmailSubject,
                "UserID": UserID,
                "ApprovalCode": ApprovalCode,
                "ApprovalStatus": ApprovalStatus,
                "ApprovalType": ApprovalType,
                "ApprovalUserID": ApprovalUserID,
            })
    }

    SaveToAIFCompany(XAgentID, ApprovalID) {                                                   
        return this.endpoint.post('/API/AgencyInformationFiles/SaveToAIFCompany')                                   
            .set('content-type', 'application/x-www-form-urlencoded')                       
            .set('authorization', 'Bearer ' + global.token)
            .send({    
                "XAgentID": XAgentID,                                                                     
                "ApprovalID": ApprovalID
            })
    }
}

module.exports = CreateAgent                                                                    // Sesuaikan dengan nama module / class nya