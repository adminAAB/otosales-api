const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (3 Parameter)", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID, AgentID, and Name)", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, process.env.Name, "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID, AgentID, and BranchCode)", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, "", process.env.BranchCode, "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID, AgentID, and AgentLevel)", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, "", "", process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID, AgentID, and AgentRole)", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, "", "", "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID, AgentID, and AgentType) #20220127003600", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, "", "", "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID, AgentID, and Evaluation Status) #20220127003630", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, "", "", "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID, Name, and BranchCode) #20220127003700", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", process.env.Name, process.env.BranchCode, "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by XAgentID, Name, and AgentLevel) #20220127003730", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", process.env.Name, "", process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 9
   it("Should passed (Search by XAgentID, Name, and AgentRole) #20220127003800", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", process.env.Name, "", "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 10
   it("Should passed (Search by XAgentID, Name, and AgentType) #20220127003830", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", process.env.Name, "", "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by XAgentID, Name, and Evaluation Status) #20220127003900", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", process.env.Name, "", "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by XAgentID, BranchCode, and AgentLevel) #20220127003930", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", process.env.BranchCode, process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by XAgentID, BranchCode, and AgentRole) #20220127004000", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", process.env.BranchCode, "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })


   //Test Case 14
   it("Should passed (Search by XAgentID, BranchCode, and AgentType) #20220127004030", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", process.env.BranchCode, "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by XAgentID, BranchCode, and Evaluation Status) #20220127004100", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", process.env.BranchCode, "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by XAgentID, AgentLevel, and AgentRole) #20220127004130", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", process.env.AgentLevel, process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by XAgentID, AgentLevel, and AgentType) #20220127004200", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", process.env.AgentLevel, "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by XAgentID, AgentLevel, and Evaluation Status) #20220127004230", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", process.env.AgentLevel, "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by XAgentID, AgentRole, and AgentType) #20220127004300", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", process.env.AgentRole, process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by XAgentID, AgentRole, and Evaluation Status) #20220127004330", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", process.env.AgentRole, "", "", process.env.EvaluationStatus, "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by XAgentID, AgentType, and Evaluation Status) #20220127004400", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", "", process.env.AgentType, process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by AgentID, Name, and BranchCode) #20220127004430", async function () {
      let response = await api_general.Search("", process.env.AgentID, process.env.Name, process.env.BranchCode, "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by AgentID, Name, and AgentLevel) #20220127004500", async function () {
      let response = await api_general.Search("", process.env.AgentID, process.env.Name, "", process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by AgentID, Name, and AgentRole) #20220127004530", async function () {
      let response = await api_general.Search("", process.env.AgentID, process.env.Name, "", "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by AgentID, Name, and AgentType) #20220127004600", async function () {
      let response = await api_general.Search("", process.env.AgentID, process.env.Name, "", "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by AgentID, Name, and Evaluation Status) #20220127004630", async function () {
      let response = await api_general.Search("", process.env.AgentID, process.env.Name, "", "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 27
   it("Should passed (Search by AgentID, BranchCode, and AgentLevel) #20220127004700", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", process.env.BranchCode, process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by AgentID, BranchCode, and AgentRole) #20220127004730", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", process.env.BranchCode, "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 29
   it("Should passed (Search by AgentID, BranchCode, and AgentType) #20220127004800", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", process.env.BranchCode, "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 30
   it("Should passed (Search by AgentID, BranchCode, and Evaluation Status) #20220127004830", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", process.env.BranchCode, "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 31
   it("Should passed (Search by AgentID, AgentLevel, and AgentRole) #20220127004900", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", process.env.AgentLevel, process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 32
   it("Should passed (Search by AgentID, AgentLevel, and AgentType) #20220127004930", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", process.env.AgentLevel, "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 33
   it("Should passed (Search by AgentID, AgentLevel, and Evaluation Status) #20220127005000", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", process.env.AgentLevel, "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 34
   it("Should passed (Search by AgentID, AgentRole, and AgentType) #20220127005030", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", "", process.env.AgentRole, process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 35
   it("Should passed (Search by AgentID, AgentRole, and Evaluation Status) #20220127005100", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", "", process.env.AgentRole, "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 36
   it("Should passed (Search by AgentID, AgentType, and Evaluation Status) #20220127005130", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", "", "", process.env.AgentType, process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 37
   it("Should passed (Search by Name, BranchCode, and AgentLevel) #20220127005200", async function () {
      let response = await api_general.Search("", "", process.env.Name, process.env.BranchCode, process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 38
   it("Should passed (Search by Name, BranchCode, and AgentRole) #20220127005230", async function () {
      let response = await api_general.Search("", "", process.env.Name, process.env.BranchCode, "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 39
    it("Should passed (Search by Name, BranchCode, and AgentType) #20220127005300", async function () {
      let response = await api_general.Search("", "", process.env.Name, process.env.BranchCode, "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 40
    it("Should passed (Search by Name, BranchCode, and Evaluation Status) #20220127005330", async function () {
      let response = await api_general.Search("", "", process.env.Name, process.env.BranchCode, "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 41
   it("Should passed (Search by Name, AgentLevel, and AgentRole) #20220127005400", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", process.env.AgentLevel, process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 42
   it("Should passed (Search by Name, AgentLevel, and AgentType) #20220127005430", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", process.env.AgentLevel, "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 43
   it("Should passed (Search by Name, AgentLevel, and Evaluation Status) #20220127005500", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", process.env.AgentLevel, "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 44
    it("Should passed (Search by Name, AgentRole, and AgentType) #20220127005530", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", "", process.env.AgentRole, process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 45
   it("Should passed (Search by Name, AgentRole, and Evaluation Status) #20220127005600", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", "", process.env.AgentRole, "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 46
   it("Should passed (Search by Name, AgentType, and Evaluation Status) #20220127005630", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", "", "", process.env.AgentType, process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 47
   it("Should passed (Search by BranchCode, AgentLevel, and AgentRole) #20220127005700", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, process.env.AgentLevel, process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 48
   it("Should passed (Search by BranchCode, AgentLevel, and AgentType) #20220127005730", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, process.env.AgentLevel, "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 49
   it("Should passed (Search by BranchCode, AgentLevel, and Evaluation Status) #20220127005800", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, process.env.AgentLevel, "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 50
   it("Should passed (Search by BranchCode, AgentRole, and AgentType) #20220127005830", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, "", process.env.AgentRole, process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 51
   it("Should passed (Search by BranchCode, AgentRole, and Evaluation Status) #20220127005900", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, "", process.env.AgentRole, "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

    //Test Case 52
    it("Should passed (Search by BranchCode, AgentType, and Evaluation Status) #20220127005930", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, "", "", process.env.AgentType, process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 53
   it("Should passed (Search by AgentLevel, AgentRole, and AgentType) #20220127010000", async function () {
      let response = await api_general.Search("", "", "", "", process.env.AgentLevel, process.env.AgentRole, process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 54
   it("Should passed (Search by AgentLevel, AgentRole, and Evaluation Status) #20220127010030", async function () {
      let response = await api_general.Search("", "", "", "", process.env.AgentLevel, process.env.AgentRole, "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 55
   it("Should passed (Search by AgentLevel, AgentType, and Evaluation Status) #20220127010100", async function () {
      let response = await api_general.Search("", "", "", "", process.env.AgentLevel, "", process.env.AgentType, process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 56
   it("Should passed (Search by AgentRole, AgentType, and Evaluation Status) #20220127010130", async function () {
      let response = await api_general.Search("", "", "", "", "", process.env.AgentRole, process.env.AgentType, process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })
})