const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (5 Parameter) Data Not Found", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })


   //Test Case 8
   it("Should passed (Search by XAgentID, AgentID, Name, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })


   //Test Case 9
   it("Should passed (Search by XAgentID, AgentID, Name, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })


   //Test Case 10
   it("Should passed (Search by XAgentID, AgentID, Name, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Alalalulu", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })


   //Test Case 14
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by XAgentID, AgentID, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })
   
   //Test Case 27
   it("Should passed (Search by XAgentID, Name, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by XAgentID, Name, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 29
   it("Should passed (Search by XAgentID, Name, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 30
   it("Should passed (Search by XAgentID, Name, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Alalalulu", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 31
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 32
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 33
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 34
   it("Should passed (Search by XAgentID, BranchCode, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 35
   it("Should passed (Search by XAgentID, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 36
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 37
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 38
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 39
    it("Should passed (Search by AgentID, Name, BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 40
    it("Should passed (Search by AgentID, Name, BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 41
   it("Should passed (Search by AgentID, Name, BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 42
   it("Should passed (Search by AgentID, Name, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 43
   it("Should passed (Search by AgentID, Name, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 44
    it("Should passed (Search by AgentID, Name, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 45
   it("Should passed (Search by AgentID, Name, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Alalalulu", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 46
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 47
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 48
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 49
   it("Should passed (Search by AgentID, BranchCode, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 50
   it("Should passed (Search by AgentID, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 51
   it("Should passed (Search by Name, BranchCode, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Alalalulu", "122", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 52
    it("Should passed (Search by Name, BranchCode, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Alalalulu", "122", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 53
   it("Should passed (Search by Name, BranchCode, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Alalalulu", "122", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 54
   it("Should passed (Search by Name, BranchCode, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Alalalulu", "122", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 55
   it("Should passed (Search by Name, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Alalalulu", "", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 56
   it("Should passed (Search by BranchCode, AgentLevel, AgentRole, AgentType, Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })
})