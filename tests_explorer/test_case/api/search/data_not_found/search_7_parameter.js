require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (7 Parameter) Data Not Found", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "100", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "100", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "100", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID, AgentID, Name, BranchCode, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "022", "", "100", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID, AgentID, Name, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "Lily Juliana", "", "1", "100", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID, AgentID, BranchCode, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0014", "00000014", "", "022", "1", "1", "100", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID, Name, BranchCode, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS0014", "", "Lily Juliana", "022", "1", "100", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by AgentID, Name, BranchCode, AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "00000014", "Lily Juliana", "022", "1", "100", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })
})