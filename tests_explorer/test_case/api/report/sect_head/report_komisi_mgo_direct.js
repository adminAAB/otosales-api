require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Generate Report Komisi MGO (Direct)", function () {

   before(async function () {
      await initial.login(process.env.USER_SECTHEAD, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Generate Report Komisi MGO (Direct) Weekly) #20220127001300", async function () {
      let response = await api_general.GenerateReport("1", "2022-01-01", "2022-01-07");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })

   //Test Case 2
   it("Should passed (Generate Report Komisi MGO (Direct) Monthly) #20220127001330", async function () {
      let response = await api_general.GenerateReport("1", "2022-01-01", "2022-01-31");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })
})