const ApiBase = require(`${process.cwd()}/tests_explorer/object_repository/api_base`)       // Harus selalu ada

class General extends ApiBase {                                                             // Sesuaikan nama module nya
    Login(username, password, biztype) {                                                    // Sesuaikan nama function dengan nama API, parameternya disesuaikan juga
        return this.endpoint.post('/API/root/signin')                                       // Sesuaikan dengan path URL API nya
            .set('content-type', 'application/x-www-form-urlencoded')                       // Sesuaikan content-type nya
            .send({                                                                         // Sesuaikan parameter nya
                "username": username,
                "password": password,
                "biztype": biztype
            })
    }

    Search(XAgentID, AgentID, Name, BranchCode, AgentLevel, AgentRole, AgentType, EvalStatus, PageNo, RowCount, OrderBy, OrderDirection) {                                        // Sesuaikan nama function dengan nama API, parameternya disesuaikan juga
        return this.endpoint.post('/API/AgencyInformationFiles/GetAgentList')               // Sesuaikan dengan path URL API nya
            .set('content-type', 'application/x-www-form-urlencoded')                       // Sesuaikan content-type nya
            .set('authorization', 'Bearer ' + global.token)                                 //Authorization Code
            .send({                                                                         // Sesuaikan parameter nya
                "SearchParams[XAgentID]": XAgentID,
                "SearchParams[AgentID]": AgentID,
                "SearchParams[Name]": Name,
                "SearchParams[BranchCode]": BranchCode,
                "SearchParams[AgentLevel]": AgentLevel,
                "SearchParams[AgentRole]": AgentRole,
                "SearchParams[AgentType]": AgentType,
                "SearchParams[EvalStatus]": EvalStatus,
                "PageNo": PageNo,
                "RowCount": RowCount,
                "OrderBy": OrderBy,
                "OrderDirection": OrderDirection
            })
    }

    GenerateReport(ReportType, PeriodFrom, PeriodTo) {                                                    // Sesuaikan nama function dengan nama API, parameternya disesuaikan juga
        return this.endpoint.post('/API/AgencyInformationFiles/RequestGenerateEmailReport')               // Sesuaikan dengan path URL API nya
            .set('content-type', 'application/x-www-form-urlencoded')                       // Sesuaikan content-type nya
            .set('authorization', 'Bearer ' + global.token)                                 //Authorization Code
            .send({                                                                         // Sesuaikan parameter nya
                "ReportType": ReportType,
                "PeriodFrom": PeriodFrom,
                "PeriodTo": PeriodTo
            })
    }

    GenerateReportOP(ReportType, PeriodFrom, PeriodTo,RemarkPeriod) {                                                    // Sesuaikan nama function dengan nama API, parameternya disesuaikan juga
        return this.endpoint.post('/API/AgencyInformationFiles/RequestGenerateEmailReport')               // Sesuaikan dengan path URL API nya
            .set('content-type', 'application/x-www-form-urlencoded')                       // Sesuaikan content-type nya
            .set('authorization', 'Bearer ' + global.token)                                 //Authorization Code
            .send({                                                                         // Sesuaikan parameter nya
                "ReportType": ReportType,
                "PeriodFrom": PeriodFrom,
                "PeriodTo": PeriodTo,
                "RemarkPeriod" : RemarkPeriod
            })
    }
}

module.exports = General                                                                    // Sesuaikan dengan nama module / class nya