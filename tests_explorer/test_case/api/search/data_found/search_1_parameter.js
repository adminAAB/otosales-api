require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (1 Parameter)", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID) #20220127001800", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by AgentID) #20220127001830", async function () {
      let response = await api_general.Search("", process.env.AgentID, "", "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by Name) #20220127001900", async function () {
      let response = await api_general.Search("", "", process.env.Name, "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })
   

   //Test Case 4
   it("Should passed (Search by BranchCode) #20220127001930", async function () {
      let response = await api_general.Search("", "", "", process.env.BranchCode, "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by AgentLevel) #20220127002000", async function () {
      let response = await api_general.Search("", "", "", "", process.env.AgentLevel, "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by AgentRole) 20220127002030", async function () {
      let response = await api_general.Search("", "", "", "", "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by AgentType) #20220127002100", async function () {
      let response = await api_general.Search("", "", "", "", "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by Evaluation Status) #20220127002130", async function () {
      let response = await api_general.Search("", "", "", "", "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })
})