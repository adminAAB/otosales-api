require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Generate Report Total FL & DL", function () {

   before(async function () {
      await initial.login(process.env.USER_AGENCYMGR, process.env.PASS, 1);
  });

   //Test Case 1
   it("Should passed (Generate Report Total FL & DL (Month To Date)) #20220127001130", async function () {
      let response = await api_general.GenerateReportOP("5", "2022-01-01", "2022-01-31","MTD");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })

   //Test Case 2
   it("Should passed (Generate Report Total FL & DL (Year To Date)) #20220127001200", async function () {
      let response = await api_general.GenerateReportOP("5", "2022-01-31", "2022-01-31","YTD");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })
})