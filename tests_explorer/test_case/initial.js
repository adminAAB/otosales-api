require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
   , api_general = new apis.apiGeneral

/* Function login yang akan dipanggil di before di dalam describe */
async function login(username, password, biztype) {
   let response = await api_general.Login(username, password, biztype);
   response.should.have.status(200);
   response.body.status.should.equal(true);
   response.body.errorCode.should.equal(0);
   response.body.token.should.not.equal(null);
   global.token = response.body.token;
}

/* Function pageGuid yang akan dipanggil di before di dalam describe */
async function getPageGuid(username, password, biztype) {
   let response = await api_general.Login(username, password, biztype);
   response.should.have.status(200);
   response.body.status.should.equal(true);
   response.body.errorCode.should.equal(0);
   response.body.token.should.not.equal(null);
   global.pageGuid = response.body.pageGuid;
}

module.exports = {
   login,
   getPageGuid
}