require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Generate Report Order & Produksi Agency", function () {

   before(async function () {
      await initial.login(process.env.USER_AGENCYMGR, process.env.PASS, 1);
  });

   //Test Case 1
   it("Should passed (Generate Report Order & Produksi Agency (By Order Date) Weekly) #20220127000900", async function () {
      let response = await api_general.GenerateReportOP("4", "2022-01-01", "2022-01-07","OrderDate");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })

   //Test Case 2
   it("Should passed (Generate Report Order & Produksi Agency (By Order Date) Monthly) #20220127000930", async function () {
      let response = await api_general.GenerateReport("4", "2022-01-01", "2022-01-31","OrderDate");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })

   //Test Case 3
   it("Should passed (Generate Report Order & Produksi Agency (By Production Date) Monthly) #20220127001000", async function () {
      let response = await api_general.GenerateReport("4", "2022-01-01", "2022-01-31","ProductionDate");
      response.should.have.status(200);
      response.body.status.should.equal(1);
   })
})