const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (2 Parameter)", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID and AgentID) #20220127002200", async function () {
      let response = await api_general.Search(process.env.XAgentID, process.env.AgentID, "", "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID and Name) #20220127002230", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", process.env.Name, "", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID and BranchCode) #20220127002300", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", process.env.BranchCode, "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID and Agent Level) #20220127002330", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID and AgentRole) #20220127002400", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", process.env.AgentRole, "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID and AgentType) #20220127002430", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", "", process.env.AgentType, "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID and Evaluation Status) #20220127002500", async function () {
      let response = await api_general.Search(process.env.XAgentID, "", "", "", "", "", "", process.env.EvaluationStatus, "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by AgentID and Name) #20220127002530", async function () {
      let response = await api_general.Search("", process.env.AgentID, process.env.Name, "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 9
   it("Should passed (Search by AgentID and BranchCode) #20220127002600", async function(){
      let response = await api_general.Search("",process.env.AgentID,"",process.env.BranchCode,"","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 10
   it("Should passed (Search by AgentID and AgentLevel) #20220127002630", async function(){
      let response = await api_general.Search("",process.env.AgentID,"","",process.env.AgentLevel,"","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by AgentID and AgentRole) #20220127002700", async function(){
      let response = await api_general.Search("",process.env.AgentID,"","","",process.env.AgentRole,"","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by AgentID and AgentType) #20220127002730", async function(){
      let response = await api_general.Search("",process.env.AgentID,"","","","",process.env.AgentType,"","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by AgentID and Evaluation Status) #20220127002800", async function(){
      let response = await api_general.Search("",process.env.AgentID,"","","","","",process.env.EvaluationStatus,"1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 14
   it("Should passed (Search by Name and BranchCode) #20220127002830", async function(){
      let response = await api_general.Search("","",process.env.Name,process.env.BranchCode,"","","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by Name and AgentLevel) #20220127002900", async function(){
      let response = await api_general.Search("","",process.env.Name,"",process.env.AgentLevel,"","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by Name and AgentRole) #20220127002930", async function(){
      let response = await api_general.Search("","",process.env.Name,"","",process.env.AgentRole,"","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by Name and AgentType) #20220127003000", async function(){
      let response = await api_general.Search("","",process.env.Name,"","","",process.env.AgentType,"","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by Name and Evaluation Status) #20220127003030", async function(){
      let response = await api_general.Search("","",process.env.Name,"","","","",process.env.EvaluationStatus,"1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by BranchCode and AgentLevel) #20220127003100", async function(){
      let response = await api_general.Search("","","",process.env.BranchCode,process.env.AgentLevel,"","","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by BranchCode and AgentRole) #20220127003130", async function(){
      let response = await api_general.Search("","","",process.env.BranchCode,"",process.env.AgentRole,"","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by BranchCode and AgentType) #20220127003200", async function(){
      let response = await api_general.Search("","","",process.env.BranchCode,"","",process.env.AgentType,"","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by BranchCode and Evaluation Status) #20220127003230", async function(){
      let response = await api_general.Search("","","",process.env.BranchCode,"","","",process.env.EvaluationStatus,"1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by AgentLevel and AgentRole) #20220127003300", async function(){
      let response = await api_general.Search("","","","",process.env.AgentLevel,process.env.AgentRole,"","","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by AgentLevel and AgentType) #20220127003330", async function(){
      let response = await api_general.Search("","","","",process.env.AgentLevel,"",process.env.AgentType,"","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by AgentLevel and Evaluation Status) #20220127003400", async function(){
      let response = await api_general.Search("","","","",process.env.AgentLevel,"","",process.env.EvaluationStatus,"1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by AgentRole and AgentType) #20220127003430", async function(){
      let response = await api_general.Search("","","","","",process.env.AgentRole,process.env.AgentType,"","1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 27
   it("Should passed (Search by AgentRole and Evaluation Status) #20220127003500", async function(){
      let response = await api_general.Search("","","","","",process.env.AgentRole,"",process.env.EvaluationStatus,"1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by AgentType and Evaluation Status) #20220127003530", async function(){
      let response = await api_general.Search("","","","","","",process.env.AgentType,process.env.EvaluationStatus,"1","10","XAgentID","ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.not.equal(0);
   })
})