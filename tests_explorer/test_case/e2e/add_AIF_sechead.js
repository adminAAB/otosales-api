//const path = require("path/posix");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , api_createagent = new apis.apiCreateAgent
  , db = require(`${process.cwd()}/tests_explorer/utils/database`)
  , generate = require(`${process.cwd()}/tests_explorer/utils/generate_data`)
  , excel = require(`${process.cwd()}/tests_explorer/utils/excel`)
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)

let email = faker.internet.email();
let address = faker.address.streetName();
let text = generate.randomString(10);

let EmailBody1, EmailSubject1, UserID1, ApprovalCode1, ApprovalStatus1, ApprovalType1, ApprovalUserID1;

//Path 
let pathAddAIF = './tests_explorer/data/sechead/Add Agency Information Files (Sect Head).xlsx';
let pathSaveApprovalInfo = './tests_explorer/data/sechead/Save Approval Info (Sect Head).xlsx';
let pathUploadDocument = './tests_explorer/data/sechead/Upload Image (Document).xlsx';
let pathUploadImage = './tests_explorer/data/sechead/Upload Image (Image).xlsx';
let pathDocument = './tests_explorer/data/upload/AutomateDocument.pdf';
let pathImage = './tests_explorer/data/upload/AutomateImage.jpg';

//Date
let nowDate = new Date();
let thisyear = nowDate.getFullYear()+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let lastyear = (nowDate.getFullYear() - 1)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let nextyear = (nowDate.getFullYear() + 1)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let birthyear = (nowDate.getFullYear() - 22)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let edusyear = (nowDate.getFullYear() - 12)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let edueyear = (nowDate.getFullYear() - 8)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let worksyear = (nowDate.getFullYear() - 7)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let workeyear = (nowDate.getFullYear() - 5)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let coursesyear = (nowDate.getFullYear() - 4)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);
let courseeyear = (nowDate.getFullYear() - 2)+'-'+("0" + (nowDate.getMonth()+1)).slice(-2)+'-'+("0" + (nowDate.getDate())).slice(-2);

describe("Test Suite Add AIF by Admin and Approve by Sect Head", function () {
   before(async function () {
      await initial.login(process.env.USER_SECTHEAD, process.env.PASS, 1);
   });

   //Test Case 1
   it("should get email Gomitra", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         let response = await api_createagent.GetEmailGomitra(excel.readSingleCell(pathAddAIF,5,row))
         response.should.have.status(200);
         response.body.status.should.equal(1);

         //Insert Email to Excel
         excel.writeSingleCell(pathAddAIF, 16, row, response.body.data)
      }
   })
   
   //Test Case 2
   it("should add AIF", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF);
      let rowDocument = excel.getRowNumbers(pathUploadDocument);
      let rowImage = excel.getRowNumbers(pathUploadImage);
      for(let row = 2; row <= rowNumbers+1; row++){
         
         //Clear XAgentID dan PKSNo (Karena XAgentID dan PKSNo auto generate sehingga tidak dikirimkan saat request parameter)
         excel.writeSingleCell(pathAddAIF, 4, row, '')
         excel.writeSingleCell(pathAddAIF, 13, row, '')

         //Update Column Excel yang mempunyai format Date
         excel.writeSingleCell(pathAddAIF, 10, row, thisyear)
         excel.writeSingleCell(pathAddAIF, 11, row, Math.floor(100000000000000 + Math.random() * 899999999999999))
         excel.writeSingleCell(pathAddAIF, 12, row, thisyear)
         excel.writeSingleCell(pathAddAIF, 14, row, nextyear)
         excel.writeSingleCell(pathAddAIF, 24, row, Math.floor(100000000000000 + Math.random() * 899999999999999))
         excel.writeSingleCell(pathAddAIF, 27, row, lastyear)
         excel.writeSingleCell(pathAddAIF, 32, row, Math.floor(1000000000000000 + Math.random() * 8999999999999999))
         excel.writeSingleCell(pathAddAIF, 34, row, birthyear)
         excel.writeSingleCell(pathAddAIF, 71, row, edusyear)
         excel.writeSingleCell(pathAddAIF, 72, row, edueyear)
         excel.writeSingleCell(pathAddAIF, 75, row, worksyear)
         excel.writeSingleCell(pathAddAIF, 76, row, workeyear)
         excel.writeSingleCell(pathAddAIF, 81, row, coursesyear)
         excel.writeSingleCell(pathAddAIF, 82, row, courseeyear)

         //Read Excel File
         let IsMGOBid = excel.readSingleCell(pathAddAIF, 1, row);
         let RegNo = '';
         let AifID = '';
         let XAgentID = '';
         let Name = excel.readSingleCell(pathAddAIF, 5, row);
         let AgentNameBukuTabungan = excel.readSingleCell(pathAddAIF, 6, row);
         let NName = excel.readSingleCell(pathAddAIF, 7, row);
         let BranchCode = excel.readSingleCell(pathAddAIF, 8, row);
         let AgentType = excel.readSingleCell(pathAddAIF, 9, row);
         let RegDate = excel.readSingleCell(pathAddAIF, 10, row);
         let AAUICertNo = excel.readSingleCell(pathAddAIF, 11, row);
         let AAUICertExpDate = excel.readSingleCell(pathAddAIF, 12, row);
         let PKSNo = excel.readSingleCell(pathAddAIF, 13, row);
         let PKSExpDate = excel.readSingleCell(pathAddAIF, 14, row);
         let UAgentID = excel.readSingleCell(pathAddAIF, 15, row);
         let GomitraEmail = excel.readSingleCell(pathAddAIF, 16, row);
         let AppDate = '';
         let BankID = excel.readSingleCell(pathAddAIF, 18, row);
         let BankName = excel.readSingleCell(pathAddAIF, 19, row);
         let BankBranchID = excel.readSingleCell(pathAddAIF, 20, row);
         let BankBranch = excel.readSingleCell(pathAddAIF, 21, row);
         let BankAccount = excel.readSingleCell(pathAddAIF, 22, row);
         let AccountName = excel.readSingleCell(pathAddAIF, 23, row);
         let NPWPNo = excel.readSingleCell(pathAddAIF, 24, row);
         let NPWPName = excel.readSingleCell(pathAddAIF, 25, row);
         let NPWPAddress = excel.readSingleCell(pathAddAIF, 26, row);
         let NPWPDate = excel.readSingleCell(pathAddAIF, 27, row);
         let Dependant = excel.readSingleCell(pathAddAIF, 28, row);
         let IsLeader = excel.readSingleCell(pathAddAIF, 29, row);
         let ReferenceAgent = '';
         let IdentityType = excel.readSingleCell(pathAddAIF, 31, row);
         let IdentityNo = excel.readSingleCell(pathAddAIF, 32, row);
         let BirthPlace = excel.readSingleCell(pathAddAIF, 33, row);
         let BirthDate = excel.readSingleCell(pathAddAIF, 34, row);
         let Gender = excel.readSingleCell(pathAddAIF, 35, row);
         let Marital = excel.readSingleCell(pathAddAIF, 36, row);
         let CitizenShip = excel.readSingleCell(pathAddAIF, 37, row);
         let Religion = excel.readSingleCell(pathAddAIF, 38, row);
         let MobilePhone = excel.readSingleCell(pathAddAIF, 39, row);
         let MobilePhone2 = excel.readSingleCell(pathAddAIF, 40, row);
         let MobilePhone3 = excel.readSingleCell(pathAddAIF, 41, row);
         let Email = excel.readSingleCell(pathAddAIF, 42, row);
         let HAddress = excel.readSingleCell(pathAddAIF, 43, row);
         let HAddress2 = excel.readSingleCell(pathAddAIF, 44, row);
         let HCity = excel.readSingleCell(pathAddAIF, 45, row);
         let HZipCode = excel.readSingleCell(pathAddAIF, 46, row);
         let HPhone = excel.readSingleCell(pathAddAIF, 47, row);
         let OAddress = excel.readSingleCell(pathAddAIF, 48, row);
         let OCity = excel.readSingleCell(pathAddAIF, 49, row);
         let OZipCode = excel.readSingleCell(pathAddAIF, 50, row);
         let OExtPhone = excel.readSingleCell(pathAddAIF, 51, row);
         let OEMail = excel.readSingleCell(pathAddAIF, 52, row);
         let RName = excel.readSingleCell(pathAddAIF, 53, row);
         let RAddress = excel.readSingleCell(pathAddAIF, 54, row);
         let RCity = excel.readSingleCell(pathAddAIF, 55, row);
         let RZipCode = excel.readSingleCell(pathAddAIF, 56, row);
         let RPhone = excel.readSingleCell(pathAddAIF, 57, row);
         let RMobilePhone = excel.readSingleCell(pathAddAIF, 58, row);
         let REMail = excel.readSingleCell(pathAddAIF, 59, row);
         let FName = excel.readSingleCell(pathAddAIF, 60, row);
         let FAddress = excel.readSingleCell(pathAddAIF, 61, row);
         let FCity = excel.readSingleCell(pathAddAIF, 62, row);
         let FZipCode = excel.readSingleCell(pathAddAIF, 63, row);
         let FPhone = excel.readSingleCell(pathAddAIF, 64, row);
         let FMobilePhone = excel.readSingleCell(pathAddAIF, 65, row);
         let FEMail = excel.readSingleCell(pathAddAIF, 66, row);
         let EduLevel = excel.readSingleCell(pathAddAIF, 67, row);
         let Institution = excel.readSingleCell(pathAddAIF, 68, row);
         let Program = excel.readSingleCell(pathAddAIF, 69, row);
         let City = excel.readSingleCell(pathAddAIF, 70, row);
         let EduSDate = excel.readSingleCell(pathAddAIF, 71, row);
         let EduEDate = excel.readSingleCell(pathAddAIF, 72, row);
         let WETitle = excel.readSingleCell(pathAddAIF, 73, row);
         let WECompany = excel.readSingleCell(pathAddAIF, 74, row);
         let WEWExSDate = excel.readSingleCell(pathAddAIF, 75, row);
         let WEWExEDate = excel.readSingleCell(pathAddAIF, 76, row);
         let CMCourseName = excel.readSingleCell(pathAddAIF, 77, row);
         let CMOrganizer = excel.readSingleCell(pathAddAIF, 78, row);
         let CMCategory = excel.readSingleCell(pathAddAIF, 79, row);
         let CMResult = excel.readSingleCell(pathAddAIF, 80, row);
         let CMCourseSDate = excel.readSingleCell(pathAddAIF, 81, row);
         let CMCourseEDate = excel.readSingleCell(pathAddAIF, 82, row);
         let UserID = excel.readSingleCell(pathAddAIF, 83, row);
         let UserRole = excel.readSingleCell(pathAddAIF, 84, row);
         let ApprovalType = excel.readSingleCell(pathAddAIF, 85, row);

         let response = await api_createagent.SaveAgentInfo(IsMGOBid, RegNo, AifID, XAgentID, Name, AgentNameBukuTabungan, NName, BranchCode, AgentType, RegDate, AAUICertNo,
            AAUICertExpDate, PKSNo, PKSExpDate, UAgentID, GomitraEmail, AppDate, BankID, BankName, BankBranchID, BankBranch, BankAccount, 
            AccountName, NPWPNo, NPWPName, NPWPAddress, NPWPDate, Dependant, IsLeader, ReferenceAgent, IdentityType, IdentityNo, BirthPlace,
            BirthDate, Gender, Marital, CitizenShip, Religion, MobilePhone, MobilePhone2, MobilePhone3, Email, HAddress, HAddress2, HCity, HZipCode,
            HPhone, OAddress, OCity, OZipCode, OExtPhone, OEMail, RName, RAddress, RCity, RZipCode, RPhone, RMobilePhone, REMail, FName, FAddress,
            FCity, FZipCode, FPhone, FMobilePhone, FEMail, EduLevel, Institution, Program, City, EduSDate, EduEDate, WETitle, WECompany,WEWExSDate,
            WEWExEDate, CMCourseName, CMOrganizer, CMCategory, CMResult, CMCourseSDate, CMCourseEDate, UserID, UserRole, ApprovalType);
            response.should.have.status(200);
            response.body.status.should.equal(1);

         //Add XAgentID and ApprovalID for Approval
         excel.writeSingleCell(pathSaveApprovalInfo, 1, row, response.body.data.XAgentID)
         excel.writeSingleCell(pathSaveApprovalInfo, 2, row, response.body.data.ApprovalID)

         //Add XAgentID in Add AIF
         excel.writeSingleCell(pathAddAIF, 4, row, response.body.data.XAgentID)
         
         //Input ReferenceNo for Upload Image
         for(let row_d = 2; row_d <= rowDocument+1; row_d++){
            excel.writeSingleCell(pathUploadDocument, 4, row_d, response.body.data.ID);
            excel.writeSingleCell(pathUploadDocument, 7, row_d, response.body.data.ApprovalID);
         }

         for(let row_i = 2; row_i <= rowImage+1; row_i++){
            excel.writeSingleCell(pathUploadImage, 4, row_i, response.body.data.ID)
            excel.writeSingleCell(pathUploadImage, 7, row_i, response.body.data.ApprovalID)
         }
      }
   })

   //Test Case 3
   it("should be Upload Data", async function () {
      let rowDocument = excel.getRowNumbers(pathUploadDocument)
      let rowImage = excel.getRowNumbers(pathUploadImage)
   
      for(let rowd = 2; rowd <= rowDocument+1; rowd++){
         //Read Excel Data
         let pDataFiles0 = pathDocument;
         let pCountDataFiles = excel.readSingleCell(pathUploadDocument, 1, rowd);
         let pObjectNo = excel.readSingleCell(pathUploadDocument, 2, rowd);
         let pReferenceImageType = excel.readSingleCell(pathUploadDocument, 3, rowd);
         let pImageReferenceNo = excel.readSingleCell(pathUploadDocument, 4, rowd);
         let pImageIsValid = excel.readSingleCell(pathUploadDocument, 5, rowd);
         let pTempOrderNoIsValid = excel.readSingleCell(pathUploadDocument, 6, rowd);
         let pApprovalID = excel.readSingleCell(pathUploadDocument, 7, rowd);

         //Upload Document
         let response = await api_createagent.UploadData(pDataFiles0, pCountDataFiles, pObjectNo, pReferenceImageType, pImageReferenceNo, pImageIsValid, pTempOrderNoIsValid, pApprovalID)
         response.should.have.status(200);
         response.body.status.should.equal(true);
      }
   
      for(let rowi = 2; rowi <= rowImage+1; rowi++){
         //Read Excel Data
         let pDataFiles0 = pathImage;
         let pCountDataFiles = excel.readSingleCell(pathUploadImage, 1, rowi);
         let pReferenceImageType = excel.readSingleCell(pathUploadImage, 2, rowi);
         let pObjectNo = excel.readSingleCell(pathUploadImage, 3, rowi);
         let pImageReferenceNo = excel.readSingleCell(pathUploadImage, 4, rowi);
         let pImageIsValid = excel.readSingleCell(pathUploadImage, 5, rowi);
         let pTempOrderNoIsValid = excel.readSingleCell(pathUploadImage, 6, rowi);
         let pApprovalID = excel.readSingleCell(pathUploadImage, 7, rowi);

         //Upload Image
         let response = await api_createagent.UploadData(pDataFiles0, pCountDataFiles, pObjectNo, pReferenceImageType, pImageReferenceNo, pImageIsValid, pTempOrderNoIsValid, pApprovalID)
         response.should.have.status(200);
         response.body.status.should.equal(true);
      }
   })

   //Test Case 4
   it("should get Old Leader", async function () {
      //Get Section Head Credential
      await initial.login(process.env.USER_SECTHEAD, process.env.PASS, 1);

      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         //Read Upliner XAgentID
         let UAgentID = excel.readSingleCell(pathAddAIF, 15, row);

         let response = await api_createagent.GetOldLeader(UAgentID);
         response.should.have.status(200);
         response.body.status.should.equal(1);

         //Get Leader
         //Leader = response.body.data[0].Leader;
         excel.writeSingleCell(pathAddAIF, 87, row, response.body.data[0].Leader);
      }
   })

   //Test Case 5
   it("should be Save To AIF", async function () {
      let rowNumbers = excel.getRowNumbers(pathSaveApprovalInfo)
      for(let row = 2; row <= rowNumbers+1; row++){
         let XAgentID = excel.readSingleCell(pathSaveApprovalInfo, 1, row);
         let ApprovalID = excel.readSingleCell(pathSaveApprovalInfo, 2, row);
      
         
         let response = await api_createagent.SaveToAIF(XAgentID, ApprovalID);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 6
   it("should be Save To Sales Officer", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         let SalesOfficerID = excel.readSingleCell(pathAddAIF, 4, row);
         let Email = excel.readSingleCell(pathAddAIF, 16, row);
         let Name = excel.readSingleCell(pathAddAIF, 5, row);
         let Role = 'SALESOFFICER';
         let Class = '';
         let Password = '';
         let Validation = '';
         let Expiry = '';
         let Password_Expiry = ''; 
         let Department = '';
         let BranchCode = 'A' + excel.readSingleCell(pathAddAIF, 8, row);
         let Phone1 = excel.readSingleCell(pathAddAIF, 39, row);
         let Phone2 = excel.readSingleCell(pathAddAIF, 40, row);
         let Phone3 = excel.readSingleCell(pathAddAIF, 41, row);
         let OfficePhone = excel.readSingleCell(pathAddAIF, 51, row);
         let Ext = '';
         let Fax = '';
         let IsSendBid = excel.readSingleCell(pathAddAIF, 86, row);
         
         let response = await api_createagent.SaveSalesOfficer(SalesOfficerID, Email, Name, Role, Class, Password, Validation, Expiry, Password_Expiry, Department, 
         BranchCode, Phone1, Phone2, Phone3, OfficePhone, Ext, Fax, IsSendBid);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 7
   it("should be Save To External User", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         let UserID =excel.readSingleCell(pathAddAIF, 16, row);
         let FirstName = excel.readSingleCell(pathAddAIF, 5, row);
         let LastName = '';
         let ActivationStatus = '9';
         let ExpiryDate = '9999-12-31 00:00:00.000';
         let LastLoginDate = '';
         let AccumulatedLoginFailedCount = '0';
         let MobileNumber = excel.readSingleCell(pathAddAIF, 39, row);
         let CreatedBy = 'MDT'; 
         let IsMGOBid = excel.readSingleCell(pathAddAIF, 86, row);
         
         let response = await api_createagent.SaveExternalUser(UserID, FirstName, LastName, ActivationStatus, ExpiryDate, LastLoginDate, AccumulatedLoginFailedCount, MobileNumber, 
            CreatedBy, IsMGOBid);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 8
   it("should be Save To AIF Company", async function () {
      let rowNumbers = excel.getRowNumbers(pathSaveApprovalInfo)
      for(let row = 2; row <= rowNumbers+1; row++){
         let XAgentID = excel.readSingleCell(pathSaveApprovalInfo, 1, row);
         let ApprovalID = excel.readSingleCell(pathSaveApprovalInfo, 2, row);
      
         
         let response = await api_createagent.SaveToAIFCompany(XAgentID, ApprovalID);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 9
   it("should be Save To Customer", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         let UserID = 'MDT';
         let Cust_Id = excel.readSingleCell(pathAddAIF, 4, row);
         let Name = excel.readSingleCell(pathAddAIF, 5, row);
         let Cust_Type = excel.readSingleCell(pathAddAIF, 9, row);
         let Email = excel.readSingleCell(pathAddAIF, 42, row);
         let BirthDate = excel.readSingleCell(pathAddAIF, 34, row);
         let HomeAddress = excel.readSingleCell(pathAddAIF, 43, row);
         let HomePostalCode = excel.readSingleCell(pathAddAIF, 46, row);
         let HomeNumber = excel.readSingleCell(pathAddAIF, 47, row);
         let OfficeAddress = excel.readSingleCell(pathAddAIF, 48, row);
         let OfficePostalCode = excel.readSingleCell(pathAddAIF, 50, row);
         let OfficeNumber = excel.readSingleCell(pathAddAIF, 51, row);
         let OfficeExt = '021';
         let UplinerClientCode = excel.readSingleCell(pathAddAIF, 15, row);
         let LeaderClientCode = excel.readSingleCell(pathAddAIF, 87, row);
         let HandPhone = excel.readSingleCell(pathAddAIF, 39, row);
         let HandPhone2 = excel.readSingleCell(pathAddAIF, 40, row);
         let HandPhone3 = excel.readSingleCell(pathAddAIF, 41, row);
         let HandPhone4 = '';
         let Company_Name = excel.readSingleCell(pathAddAIF, 74, row);
         let Job_Title = excel.readSingleCell(pathAddAIF, 73, row);
         let Sex = excel.readSingleCell(pathAddAIF, 35, row);
         let Marital = excel.readSingleCell(pathAddAIF, 36, row);
         let Religion = excel.readSingleCell(pathAddAIF, 38, row);
         let id_card = excel.readSingleCell(pathAddAIF, 32, row);
         
         let response = await api_createagent.SaveCustomer(UserID, Cust_Id, Name, Cust_Type, Email, BirthDate, HomeAddress, HomePostalCode, HomeNumber, OfficeAddress, 
            OfficePostalCode, OfficeNumber, OfficeExt, UplinerClientCode, LeaderClientCode, HandPhone, HandPhone2, HandPhone3, HandPhone4, Company_Name,
            Job_Title, Sex, Marital, Religion, id_card);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 10
   it("should Record Agent Hierarchy Monthly", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         //Read Data from Excel
         let ParamSearch = excel.readSingleCell(pathAddAIF, 4, row);

         let response = await api_createagent.RecordAgentHierarchyMonthly(ParamSearch);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 11
   it("should Update Password", async function () {
      let rowNumbers = excel.getRowNumbers(pathAddAIF)
      for(let row = 2; row <= rowNumbers+1; row++){
         //Read Data from Excel
         let XAgentID = excel.readSingleCell(pathAddAIF, 4, row);
         let GomitraEmail = excel.readSingleCell(pathAddAIF, 16, row);

         let response = await api_createagent.UpdatePassword(XAgentID, GomitraEmail);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 12
   it("should send Auto Email Gomitra Account", async function () {
      let rowNumbers = excel.getRowNumbers(pathSaveApprovalInfo)
      for(let row = 2; row <= rowNumbers+1; row++){
         //Read Data from Excel
         let ApprovalID = excel.readSingleCell(pathSaveApprovalInfo, 2, row);
         let UserRole = '';

         let response = await api_createagent.SendAutoEmailAccountGomitra(ApprovalID, UserRole);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })

   //Test Case 13
   it("should get Email Detail Approval Notification", async function () {
      let rowNumbers = excel.getRowNumbers(pathSaveApprovalInfo)
      for(let row = 2; row <= rowNumbers+1; row++){
         //Read Data from Excel
         let ApprovalID = excel.readSingleCell(pathSaveApprovalInfo, 2, row);
         let UserRole = '';

         let response = await api_createagent.EmailDetailApprovalNotification(ApprovalID);
         response.should.have.status(200);
         response.body.status.should.equal(1);

         //Assign Email Detail to Variable
         EmailBody1 = response.body.data.EmailBody;
         EmailSubject1 = response.body.data.EmailSubject;
         UserID1 = response.body.data.UserID;
         ApprovalCode1 = response.body.data.ApprovalCode;
         ApprovalStatus1 = response.body.data.ApprovalStatus;
         ApprovalType1 = response.body.data.ApprovalType;
         ApprovalUserID1 = response.body.data.ApprovalUserID;
      }
   })

   //Test Case 14
   it("should get send Email Notification", async function () {
      let rowNumbers = excel.getRowNumbers(pathSaveApprovalInfo)
      for(let row = 2; row <= rowNumbers+1; row++){
         //Read Data from Excel
         let EmailBody = EmailBody1;
         let EmailSubject = EmailSubject1;
         let UserID = UserID1;
         let ApprovalCode = ApprovalCode1;
         let ApprovalStatus = ApprovalStatus1;
         let ApprovalType = ApprovalType1;
         let ApprovalUserID = ApprovalUserID1;

         let response = await api_createagent.SendEmailNotification(EmailBody, EmailSubject, UserID, ApprovalCode, ApprovalStatus, ApprovalType, ApprovalUserID);
         response.should.have.status(200);
         response.body.status.should.equal(1);
      }
   })
})