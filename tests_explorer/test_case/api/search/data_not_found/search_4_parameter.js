const { response } = require("express");

require(`${process.cwd()}/tests_explorer/base`)

const apis = require(`${process.cwd()}/tests_explorer/object_repository/apis`)
  , api_general = new apis.apiGeneral
  , initial = require(`${process.cwd()}/tests_explorer/test_case/initial`)


describe("Test Suite Search (4 Parameter) Data Not Found", function () {

   before(async function () {
      await initial.login(process.env.USER_ADMIN, process.env.PASS, 1);
  });
   
   //Test Case 1
   it("Should passed (Search by XAgentID, AgentID, Name, and BranchCode)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Lalalilulu", "122", "", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 2
   it("Should passed (Search by XAgentID, AgentID, Name, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Lalalilulu", "", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 3
   it("Should passed (Search by XAgentID, AgentID, Name, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Lalalilulu", "", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 4
   it("Should passed (Search by XAgentID, AgentID, Name, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Lalalilulu", "", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 5
   it("Should passed (Search by XAgentID, AgentID, Name, and EvaluationStatus)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "Lily Lalalilulu", "", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 6
   it("Should passed (Search by XAgentID, AgentID, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 7
   it("Should passed (Search by XAgentID, AgentID, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 8
   it("Should passed (Search by XAgentID, AgentID, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 9
   it("Should passed (Search by XAgentID, AgentID, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 10
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 11
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 12
   it("Should passed (Search by XAgentID, AgentID, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 13
   it("Should passed (Search by XAgentID, AgentID, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 14
   it("Should passed (Search by XAgentID, AgentID, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 15
   it("Should passed (Search by XAgentID, AgentID, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "10000014", "", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 16
   it("Should passed (Search by XAgentID, Name, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 17
   it("Should passed (Search by XAgentID, Name, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 18
   it("Should passed (Search by XAgentID, Name, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 19
   it("Should passed (Search by XAgentID, Name, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 20
   it("Should passed (Search by XAgentID, Name, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 21
   it("Should passed (Search by XAgentID, Name, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 22
   it("Should passed (Search by XAgentID, Name, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 23
   it("Should passed (Search by XAgentID, Name, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 24
   it("Should passed (Search by XAgentID, Name, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 25
   it("Should passed (Search by XAgentID, Name, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "Lily Lalalilulu", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 26
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 27
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 28
   it("Should passed (Search by XAgentID, BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 29
   it("Should passed (Search by XAgentID, BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 30
   it("Should passed (Search by XAgentID, BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 31
   it("Should passed (Search by XAgentID, BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 32
   it("Should passed (Search by XAgentID, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 33
   it("Should passed (Search by XAgentID, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 34
   it("Should passed (Search by XAgentID, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 35
   it("Should passed (Search by XAgentID, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("JESS1004", "", "", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 36
   it("Should passed (Search by AgentID, Name, BranchCode, and AgentLevel)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "122", "1", "", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 37
   it("Should passed (Search by AgentID, Name, BranchCode, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "122", "", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 38
   it("Should passed (Search by AgentID, Name, BranchCode, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "122", "", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 39
    it("Should passed (Search by AgentID, Name, BranchCode, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "122", "", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 40
    it("Should passed (Search by AgentID, Name, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 41
   it("Should passed (Search by AgentID, Name, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 42
   it("Should passed (Search by AgentID, Name, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 43
   it("Should passed (Search by AgentID, Name, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 44
    it("Should passed (Search by AgentID, Name, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 45
   it("Should passed (Search by AgentID, Name, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "Lily Lalalilulu", "", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 46
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 47
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 48
   it("Should passed (Search by AgentID, BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 49
   it("Should passed (Search by AgentID, BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 50
   it("Should passed (Search by AgentID, BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 51
   it("Should passed (Search by AgentID, BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

    //Test Case 52
    it("Should passed (Search by AgentID, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 53
   it("Should passed (Search by AgentID, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 54
   it("Should passed (Search by AgentID, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 55
   it("Should passed (Search by AgentID, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "10000014", "", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 56
   it("Should passed (Search by Name, BranchCode, AgentLevel, and AgentRole)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "122", "1", "1", "", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 57
   it("Should passed (Search by Name, BranchCode, AgentLevel, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "122", "1", "", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 58
   it("Should passed (Search by Name, BranchCode, AgentLevel, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "122", "1", "", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 59
   it("Should passed (Search by Name, BranchCode, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "122", "", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 60
   it("Should passed (Search by Name, BranchCode, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "122", "", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 61
   it("Should passed (Search by Name, BranchCode, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "122", "", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 62
   it("Should passed (Search by Name, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 63
   it("Should passed (Search by Name, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 64
   it("Should passed (Search by Name, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 65
   it("Should passed (Search by Name, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "Lily Lalalilulu", "", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 66
   it("Should passed (Search by BranchCode, AgentLevel, AgentRole, and AgentType)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "1", "1", "", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 67
   it("Should passed (Search by BranchCode, AgentLevel, AgentRole, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "1", "", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 68
   it("Should passed (Search by BranchCode, AgentLevel, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "1", "", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 69
   it("Should passed (Search by BranchCode, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "122", "", "1", "1", "1", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })

   //Test Case 70
   it("Should passed (Search by AgentLevel, AgentRole, AgentType, and Evaluation Status)", async function () {
      let response = await api_general.Search("", "", "", "", "100", "100", "100", "100", "1", "10", "XAgentID", "ASC");
      response.should.have.status(200);
      response.body.status.should.equal(1);
      response.body.data.Items.length.should.equal(0);
   })
   
})